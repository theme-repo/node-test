const express  = require('./express');
const config   = require('./config/server.config');
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const os       = require('os');

// Server configurations
const port       = process.env.PORT || config.server.port || 3000;

const serverInfo = `Server started
                    Host: ${os.hostname()}
                    Port: ${port}`;

process.on('uncaughtException', function(err) {
  console.log(`Uncaught exception: ${err}`)
});

mongoose.Promise = bluebird;

mongoose.connect(`mongodb://${config.db.host}/cyza`);

mongoose.connection.on('error', function(err) {
  console.log(err)
});

mongoose.connection.once('open', function() {
  console.log('Database connected');
  const app = express.init();
  
  app.listen(port, () => console.log(serverInfo));
});


