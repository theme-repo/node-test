module.exports = {
  ui: {
    port: 8080
  },
  proxy: 'localhost:3000',
  port: 5000,
  files: ['./modules/**'],
  ghostMode: {
    clicks: true,
    location: false,
    forms: true,
    scroll: true
  },
  open: false,
  notify: true,
  reloadDelay: 1000
};