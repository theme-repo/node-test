'use strict';
const _ = require('lodash');
const secrets = require('./secrets/dont.commit');

const ENV = process.env.NODE_ENV || 'development';

// Environment settings
const envConfig = {
  development: {
    server: {
      host: 'localhost',
      port: 3000
    },
    db: {
      host: 'localhost'
    }
  },
  production: {
    server: {
      host: 'localhost',
      port: 3000
    },
    db: {
      host: 'localhost'
    }
  },
};

module.exports = _.merge({}, envConfig[ENV], secrets[ENV]);

