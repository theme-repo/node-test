require("babel-polyfill");
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // devtool: 'source-map',
  context: path.resolve('./modules/client'),
  entry: {
    app: ['babel-polyfill', './app.module.js']
  },
  output: {
    path: path.resolve('./public'),
    filename: "[name].js"
  },
  resolveLoader: {root: path.resolve('../node_modules')},
  module: {
    loaders: [
      {
        test: /\.json$/, loader: 'json-loader'
      },
      {
        test: /\.html$/,
        loaders: ['html']
      },
      {
        test: /\.less$/,
        loader: "style!css!less?strictMath&noIeCompat"
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader?root=."
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components|thirdparty)/,
        loader: 'babel'
      }
    ]
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  plugins: [
    // new webpack.optimize.UglifyJsPlugin({minimize: true, mangle: false}),
    
    new webpack.optimize.OccurenceOrderPlugin(),
    
    new webpack.NoErrorsPlugin(),
    
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve('./modules/client/index.html')
    })
  ]
};
