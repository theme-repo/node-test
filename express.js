const _            = require('lodash');
const config       = require('./config/server.config');
const express      = require('express');
const session      = require('express-session');
const morgan       = require('morgan');
const bodyParser   = require('body-parser');
const passport     = require('passport');

const ENV = process.env.NODE_ENV;

function initModels() {
  const userModels = require('./modules/server/user/models/user.model');
}

function initRoutes(app) {
  const userRoutes = require('./modules/server/user/user.routes')(app);
  const authRoutes = require('./modules/server/auth/auth.routes')(app);
}

function initDevServer(app) {
  const webpack    = require('webpack');
  const compiler   = webpack(require('./config/webpack.config'));
  const middleware = require('webpack-dev-middleware');
  
  app.use(
    middleware(compiler, {
      publicPath: '/',
      quiet     : false,
      stats     : {colors: true}
    })
  );
}

module.exports.init = () => {
  const app = express();
  
  app.use(morgan('dev'));
  
  app.use(session({
    secret: config.server.sessionKey,
    cookie: {}
  }));
  
  app.use('*', function(req, res, next) {
    const user = _.get(req, 'session.passport.user');
    
    if (!user) {
      res.clearCookie('accountStatus');
    }
    
    next();
  });
  
  app.use(bodyParser.json());
  
  app.use(passport.initialize());
  
  app.use(passport.session());
  
  if (ENV === 'development') {
    initDevServer(app);
  }
  
  app.use(express.static('./public'));
  
  initModels();
  
  initRoutes(app);
  
  app.use(function(err, req, res, next) {
    res.status(500).send('Something broke!');
  });
  
  return app;
};
