import angular from 'angular';
import style from './index.less';
import config from './app.config';
import {routeAuthentication} from './app.routing';

// Modules
import core from './core/core.module';
import auth from './auth/auth.module';
import user from './user/user.module';
import layout from './layout/layout.module';
import ui from './ui/ui.module';

angular
  .module('app', [
    'app.core',
    'app.user',
    'app.auth',
    'app.layout',
    'app.ui'
  ])
  .config(config)
  .run(routeAuthentication);