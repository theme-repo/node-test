import angular from 'angular';
import router from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';
import ngMessage from 'angular-messages';
import ngCookie from 'angular-cookies';
import angularMaterial from 'angular-material';
import angularMaterialStyle from 'angular-material/angular-material.css';

const dependencies = [
  'ui.router',
  'ngMaterial',
  'ngSanitize',
  'ngMessages',
  'ngCookies',
];

export default angular.module('app.core', dependencies);