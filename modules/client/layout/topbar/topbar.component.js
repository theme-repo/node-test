import angular from 'angular';
import template from './topbar.html';
import style from './topbar.less';

export default {controller, template};

controller.$inject = ['userFactory', 'authFactory', '$scope', '$state'];

function controller(userFactory, authFactory, $scope, $state) {
  $scope.$watch(userFactory.isMember, member => {
    this.isMember = member;
  });
  
  this.logout = () => {
    authFactory
      .logout()
      .then(res => $state.go('app'));
  };
}