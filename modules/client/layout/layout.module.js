import angular from 'angular';

//Components
import topBar from './topbar/topbar.component'
import home from './home/home.component';

export default angular.module('app.layout', [])
                      .component('home', home)
                      .component('topBar', topBar);