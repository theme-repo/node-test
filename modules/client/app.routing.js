import {isEmpty} from 'lodash';

router.$inject = ['$stateProvider', '$UrlRouterProvider'];

export function router($stateProvider, $urlRouterProvider) {
  
  $urlRouterProvider.when('', '/');
  
  $stateProvider
  
  // Public Routes
    .state('app', {
      url  : '/',
      views: {
        header: {template: '<top-bar></top-bar>'},
        body  : {template: '<home></home>'}
      }
    })
    .state('app.login', {
      url  : 'login',
      views: {
        'body@': {template: '<login></login>'}
      }
    })
    .state('app.recovery', {
      url  : 'recovery',
      views: {
        'body@': {template: '<password-recovery></password-recovery>'}
      }
    })
    .state('app.reset', {
      url  : 'reset/:token',
      views: {
        'body@': {template: '<password-reset></password-reset>'}
      }
    })
    .state('app.signup', {
      url  : 'signup',
      views: {
        'body@': {template: '<signup></signup>'}
      }
    })
    .state('app.signup.finalize', {
      url  : '/finalize?fullName&address&state&city&zipCode',
      params: {
        test: null
      },
      views: {
        'body@': {template: '<signup-finalize></signup-finalize>'}
      }
    })
    
    // Member Routes
    .state('app.user', {
      url     : 'user',
      abstract: true
    })
    .state('app.user.profile', {
      url  : '/profile',
      views: {
        'body@': {template: '<user-profile></user-profile>'}
      }
    })
  
}

routeAuthentication.$inject = ['$rootScope', '$state', 'userFactory'];

export function routeAuthentication($rootScope, $state, userFactory) {
  
  $rootScope.$on('$stateChangeStart', (event, toState) => {
    const protectedRoutes = toState.name.match(/app.user\.?.*/i);
    const loginRoute      = toState.name.match(/^app.login/i);
    const signupRoute     = toState.name.match(/^app.signup$/i);
    
    if (protectedRoutes && !userFactory.isMember()) {
      event.preventDefault();
      $state.go('app');
    }
    
    if (userFactory.isMember() && (loginRoute || signupRoute)) {
      event.preventDefault();
      $state.go('app');
    }
  })
}