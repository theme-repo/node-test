import template from './profile.html';
import style from './profile.less';
import {extend} from 'lodash';

export default {template, controller};

controller.$inject = ['userFactory', '$scope'];

function controller(userFactory, $scope) {
  this.model = {};
  
  this.validateEmail = false;
  
  this.ready = false;
  
  this.$onInit = () => {
    userFactory
    
      .getProfile()
    
      .then(user => {
        this.ready = true;
        
        $scope.$apply(() => {
          this.model = extend(this.model, user);
        });
      })
    
      .catch(err => {
        console.log(err);
      });
  };
  
  this.submit = () => {
    userFactory
      
      .updateProfile(this.model)
    
      .then(res => {
        this.formMessage = 'Changes saved';
      })
      
      .catch(err => {
        console.log(err);
        this.formMessage = 'Save error';
      })
  }
}