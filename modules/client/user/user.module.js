import angular from 'angular';

//Components
import userFactory from './user.factory';
import profile from './profile/profile.component';

export default angular.module('app.user', [])
                      .factory('userFactory', userFactory)
                      .component('userProfile', profile);