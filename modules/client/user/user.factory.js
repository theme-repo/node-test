import {extend, isString, isObject, get} from 'lodash';

userFactory.$inject = ['$http', '$cookies'];

const USER_ENDPOINT = 'api/user';

function userFactory($http, $cookies) {
  const checkAccountStatus = () => $cookies.get('accountStatus');
  let userProfile          = {};
  
  return {
    getUser() {
      return userProfile;
    },
    
    setUser(profile) {
      userProfile = profile;
    },
    
    isMember() {
      return (checkAccountStatus());
    },
    
    async getProfile() {
      if (this.isMember()) {
        try {
          const endpoint         = USER_ENDPOINT;
          const {data: response} = await $http.get(endpoint);
          
          userProfile = response;
          
          return response;
        }
        catch (err) {
          
          throw new Error(err.data);
        }
      }
    },
    
    async updateProfile(fields) {
      if (this.isMember()) {
        try {
          const endpoint         = USER_ENDPOINT;
          const {data: response} = await $http.post(endpoint, fields);
          
          return response;
        }
        catch (err) {
          console.log('error', err);
          
          throw new Error(err.data);
        }
      }
    }
  };
}

export default userFactory;