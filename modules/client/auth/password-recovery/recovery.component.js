import template from './recovery.html';

export default {template, controller};

controller.$inject = ['authFactory', '$state'];

function controller(authFactory) {
  
  this.submit = () => {
    authFactory
      
      .recoverPassword(this.email)
      
      .then(
        res => {
          this.formMessage = 'Recovery email have been sent to you';
        },
        err => {
          console.log(err);
          this.formMessage = 'Recovery failed';
        }
      )
    
  };
}