import {extend, isString, isObject, get} from 'lodash';

const SIGNUP_ENDPOINT = 'api/signup';

const RESET_ENDPOINT = 'api/reset';

const LOGIN_ENDPOINT = 'api/login';

const LOGOUT_ENDPOINT = 'api/logout';

authFactory.$inject = ['$http', 'userFactory', '$cookies'];

function authFactory($http, userFactory, $cookies) {
  return {
    
    login(email, password) {
      return $http.post(LOGIN_ENDPOINT, {email, password})
    },
  
    async logout() {
      try {
        await $http.post(LOGOUT_ENDPOINT);
        
        userFactory.setUser({});
        
        return true;
      }
      
      catch(err) {
        console.log(err);
      }
    },
    
    signup(email, password) {
      return $http.post(SIGNUP_ENDPOINT, {email, password})
    },
    
    recoverPassword(email) {
      return $http.post(RESET_ENDPOINT, {email});
    },
    
    resetPassword(password, token) {
      return $http.post(`${RESET_ENDPOINT}/${token}`, {password});
    }
  };
}

export default authFactory;