import template from './password.reset.html';

export default {template, controller};

controller.$inject = ['authFactory', '$stateParams'];

function controller(authFactory, $stateParams) {
  const {token} = $stateParams;
  
  this.submit = () => {
    if(token && (this.password === this.repeatPassword)) {
      authFactory
        .resetPassword(this.password, token)
        .then(res => {
          this.formMessage = 'Password reset success!';
        })
        .catch(err => {
          this.formMessage = 'Password reset failed :(';
        })
    }
  }
}