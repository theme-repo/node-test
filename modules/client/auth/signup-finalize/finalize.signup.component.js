import template from './finalize.signup.html';
import style from './finalize.signup.less';
import _ from 'lodash';

export default {template, controller};

controller.$inject = ['userFactory', '$state', '$stateParams'];

function controller(userFactory, $state, $stateParams) {
  this.form = {};
  
  _.extend(this.form, $stateParams);
  
  this.submit = () => {
    userFactory
      
      .updateProfile(this.form)
      
      .then(() => {
        $state.go('app.user.profile');
      })
      
      .catch(err => {
        this.formMessage = 'Form submit error';
      })
  }
}