import template from './signup.html';
import style from './signup.less';

export default {controller, template};

controller.$inject = ['authFactory', '$state'];

function controller(authFactory, $state) {
  
  this.signup = () => {
    this.authenticating = true;
    
    authFactory
      .signup(this.email, this.password)
      
      .then(() => {
        this.authenticating = false;
        $state.go('app.signup.finalize');
      })
      
      .catch(err => {
        this.authenticating = false;
        this.formMessage    = err.message;
      })
  }
}