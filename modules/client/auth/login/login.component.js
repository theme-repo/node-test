import template from './login.html';
import style from './login.less';

export default {
  controller,
  template
}

controller.$inject = ['authFactory', '$state'];

function controller(authFactory, $state) {
  this.login = () => {
    this.authenticating = true;
    
    authFactory
      .login(this.email, this.password)
      
      .then(
        () => {
          this.authenticating = false;
          $state.go('app.user.profile');
        },
        err => {
          this.authenticating = false;
          this.formMessage    = err.message;
        }
      )
  };
}