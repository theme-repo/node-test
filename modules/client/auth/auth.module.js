import angular from 'angular';
import style from './auth.less';

//Components
import authFactory from './auth.factory';
import login from './login/login.component';
import signup from './signup/signup.component';
import passwordRecovery from './password-recovery/recovery.component';
import passwordReset from './password-reset/password.reset.component';
import finalizeSignup from './signup-finalize/finalize.signup.component';

export default angular.module('app.auth', [])
                      .factory('authFactory', authFactory)
                      .component('login', login)
                      .component('signup', signup)
                      .component('passwordRecovery', passwordRecovery)
                      .component('passwordReset', passwordReset)
                      .component('signupFinalize', finalizeSignup);