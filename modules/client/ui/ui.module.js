import angular from 'angular';

//Components
import menuFab from './menu-fab/menu.fab.component';

export default angular.module('app.ui', [])
                      .component('menuFab', menuFab);