import template from './menu.fab.component.html';

export default {
  bindings: {
    icon: '@',
    class: '@'
  },
  transclude: {
    menu: '?dropMenu'
  },
  controller,
  template
};

function controller() {
  this.class = this.class;
  this.toggleMenu = ($mdOpenMenu, ev) => $mdOpenMenu(ev);
}