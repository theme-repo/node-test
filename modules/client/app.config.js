import {router} from './app.routing';

appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$mdThemingProvider'];

function appConfig ($stateProvider, $urlRouterProvider, $mdThemingProvider) {
  
  router($stateProvider, $urlRouterProvider);
  
  // Angular material theme configuration
  $mdThemingProvider.theme('default')
                    .primaryPalette('blue-grey')
                    .accentPalette('cyan');
}

export default appConfig;
