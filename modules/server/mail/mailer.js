const _ = require('lodash');
const config = require('../../../config/server.config');
const nodeMailer = require('nodemailer');

const transport = nodeMailer.createTransport(`smtps://${config.mailer.user}:${config.mailer.password}@smtp.gmail.com`);

const defaultConfigs = {
  from: config.mailer.address
};

function createEmail(config, cb) {
  const mail = _.extend({}, defaultConfigs, config);
  
  transport.sendMail(mail, cb);
}

module.exports = createEmail;