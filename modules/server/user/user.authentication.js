const _       = require('lodash');
const config  = require('../../../config/server.config');
const User    = require('./models/user.model');
const profile = require('./user.profile');
const mailer  = require('../mail/mailer');
const util    = require('./util');

const SALT_LENGTH = 16;

function verifyUser(req, res, next) {
  const user = _.get(req, 'session.passport.user');
  
  if (user) {
    User
      .findOne({_id: user})
      .exec()
      .then(res => {
        if (res) next();
        else {
          res.clearCookie('accountStatus');
          req.session.destroy();
          res.redirect('/');
        }
      })
  } else {
    res.clearCookie('accountStatus');
    res.redirect('/');
  }
}

function verifyPassword(storedPassword, salt, password) {
  const inputPassword = util.sha512(salt, password);
  
  return storedPassword === inputPassword;
}

function createAccountFromEmail(email, password) {
  if (email && password) {
    const salt               = util.salt(SALT_LENGTH);
    const saltedHashPassword = util.sha512(salt, password);
    
    const user = new User({email, password: saltedHashPassword, salt});
    return user.save();
  }
}

function findUser(query) {
  return User.findOne(query).exec();
}

function signup(req, res) {
  const {email, password} = req.body;
  
  findUser({email})
    
    .then(doc => {
      if (!doc) return createAccountFromEmail(email, password);
      else res.status(400).send('Email already in use');
    })
    
    .then(user => {
      res.cookie('accountStatus', user.accountStatus);
      
      _.set(req, 'session.passport.user', user);
      
      res.status(200).send()
    })
    
    .catch(err => {
      console.log('Sign up error: ' + err);
      res.status(400).send('Sign up error');
    });
}

function login(req, res) {
  const {email, password} = req.body;
  
  if (!email) {
    return res.status(422).send('Username is empty');
  }
  
  if (!password) {
    return res.status(422).send('Password is empty');
  }
  
  findUser({email})
    .then(user => {
      const storedPassword = user.password;
      const salt           = user.salt;
      
      if (user) {
        try {
          const verified = verifyPassword(storedPassword, salt, password);
          
          if (verified) {
            _.set(req, 'session.passport.user', user);
            
            res.cookie('accountStatus', user.accountStatus);
            
            res.status(200).send()
          }
          
          else {
            res.status(400).send('Invalid User');
          }
        }
        catch (err) {
          res.status(400).send('Invalid password');
        }
      }
      else {
        res.status(400).send('User not found')
      }
    });
}

function passwordRecovery(req, res) {
  const {email} = req.body;
  
  if (!email) return res.status(422).send('Email resource is missing');
  
  findUser({email})
    
    .then(user => {
      
      if (user) {
        const emailToken = util.md5(email);
        
        user.emailToken = emailToken;
        user.save();
        
        const template = {
          to     : user.email,
          subject: 'Password Recovery',
          html   : `<a href="http://${req.headers.host}/#/reset/${emailToken}">Reset Password</a>`
        };
        
        mailer(template, function (err, done) {
          if (err) {
            console.log('Send Mail Error: ' + err);
            res.status(400).send();
          }
          if (done) {
            res.status(200).send();
          }
        });
      } else {
        console.log('Password recovery error: ' + err);
        
        return res.status(400).send();
      }
    })
  
}

function resetPassword(req, res) {
  const {token}    = req.params;
  const {password} = req.body;
  
  if (token && password) {
    findUser({emailToken: token})
      .then(user => {
        
        if (user) {
          const salt               = util.salt(SALT_LENGTH);
          const saltedHashPassword = util.sha512(salt, password);
          
          user.password   = saltedHashPassword;
          user.salt       = salt;
          user.emailToken = '';
          
          user.save();
          res.status(200).send('Password reset success');
        }
        else {
          res.status(422).send('Invalid email or expired link');
        }
      })
  }
  
  else {
    res.status(422).send('Missing required parameters')
  }
}

function logout(req, res) {
  req.session.destroy();
  res.clearCookie('accountStatus');
  res.status(200).send();
}


module.exports = {
  findUser,
  verifyUser,
  signup,
  login,
  logout,
  recovery: passwordRecovery,
  reset   : resetPassword
};