const FRONTEND_PROFILE_FIELDS = ['_id', 'fullName', 'email', 'address', 'state', 'city', 'zipCode'];

module.exports = {FRONTEND_PROFILE_FIELDS};