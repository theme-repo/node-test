const authentication = require('./user.authentication.js');
const profile        = require('./user.profile');

module.exports = function (app) {
  app.post('/api/signup', authentication.signup);
  
  app.post('/api/login', authentication.login);
  
  app.post('/api/logout', authentication.logout);
  
  app.post('/api/reset/', authentication.recovery);
  
  app.post('/api/reset/:token', authentication.reset);
  
  // Authenticated Routes
  app.use('/api/user*', authentication.verifyUser);
  
  app.post('/api/user', profile.update);
  
  app.get('/api/user', profile.get);
};
