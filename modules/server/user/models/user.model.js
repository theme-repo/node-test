const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  fullName: {
    type: String
  },
  
  salt: {
    type: String
  },
  
  email: {
    type: String,
    unique: true
  },
  
  emailToken: {
    type: String
  },
  
  password: {
    type: String
  },
  
  address: {
    type: String
  },
  
  city: {
    type: String
  },
  
  state: {
    type: String
  },
  
  zipCode: {
    type: Number
  },
  
  country: {
    type: String
  },
  
  created: {
    type: Date,
    default: Date.now
  },
  
  linkedinId: {
    type: String,
    unique: true,
    sparse: true
  },
  
  accountStatus: {
    type: String,
    default: 'signup' // [signup, complete]
  }
});

userSchema.plugin(require('mongoose-findorcreate'));

module.exports = mongoose.model('User', userSchema);