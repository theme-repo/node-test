const _      = require('lodash');
const crypto = require('crypto');
const User   = require('./models/user.model');
const util   = require('./util.js');

function getProfile(req, res) {
  const id = req.user._id;
  
  User
    .findOne({_id: id})
    
    .exec()
    
    .then(result => {
      
      if (result) {
        const profile = util.buildProfile(result._doc);
        
        res.status(200).json(profile);
      } else {
        res.status(400).send('Invalid user');
      }
      
    });
}

function updateProfile(req, res) {
  const id = req.user._id;
  const fields  = util.buildProfile(req.body);
  
  User
    
    .findOneAndUpdate({_id: id}, _.omit(fields, 'email'), {'upsert': true, new:true})
    
    .exec()
    
    .then(user => {
      
      if (!user) return res.status(400).send('User not found');
      
      // Search for duplicates before setting email field
      if (fields.email) {
        User
          .find({email: fields.email, _id: {'$ne': id}})
          .then(doc => {
            
            if (!_.isEmpty(doc)) res.status(400).send('Email already taken');
            
            if (_.isEmpty(doc)) {
              user.email = fields.email;
              user.save();
              res.status(200).send();
            }
          })
      }
      
      else {
        
        if (user.accountStatus === 'signup') {
          user.accountStatus = 'complete';
          user.save();
          
          res.cookie('accountStatus', user.accountStatus)
        }
        
        res.status(200).send();
      }
    });
}

module.exports = {
  update: updateProfile,
  get   : getProfile
};