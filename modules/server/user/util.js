const _       = require('lodash');
const crypto  = require('crypto');
const {FRONTEND_PROFILE_FIELDS} = require('./constants');

function salt(length) {
  return crypto.randomBytes(Math.ceil(length / 2))
               .toString('hex')
               .slice(0, length);
}

function sha512(password, salt) {
  var hash = crypto.createHmac('sha512', salt)
                   .update(password)
                   .digest('hex');
  
  return hash;
}

function md5(data) {
  return crypto.createHash('md5').update(data).digest("hex");
}

function extractFields(fields, validFields) {
  return _.transform(fields, (accumulator, val, key) => {
    const isValid = validFields.indexOf(key) !== -1;
    
    if (isValid) return accumulator[key] = val;
  }, {});
}

// Used to build profiles that are consistent for the frontend
function buildProfile(doc) {
  return extractFields(doc, FRONTEND_PROFILE_FIELDS);
}

module.exports = {
  extractFields,
  buildProfile,
  salt,
  sha512,
  md5
};