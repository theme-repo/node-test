const passport               = require('passport');
const _                      = require('lodash');
const config                 = require('../../../../config/server.config');
const User                   = require('../../user/models/user.model');
const LinkedInStrategy       = require('passport-linkedin-oauth2').Strategy;
const {linkedin, server}     = require('../../../../config/server.config');

const strategyConfig = {
  clientID    : linkedin.clientId,
  clientSecret: linkedin.clientSecret,
  callbackURL : `http://${config.server.host}/oauth/linkedin/callback`,
  scope       : ['r_emailaddress', 'r_basicprofile'],
};

function strategyHandler(linkedinToken, refreshToken, profile, done) {
  const json       = _.get(profile, '_json');
  const linkedinId = json.id;
  
  if (linkedinToken && linkedinId) {
    User
      .findOrCreate({linkedinId}, function (err, user, created) {
        
        if (created) {
          // Logic for populating user with linkedin data
          
          if (json.formattedName) user.fullName = json.formattedName;
          
          user.save();
        }
        
        done(null, user);
      });
  }
}

passport.use(new LinkedInStrategy(strategyConfig, strategyHandler));