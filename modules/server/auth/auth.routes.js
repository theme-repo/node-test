const _                      = require('lodash');
const qs                     = require('querystring');
const passport               = require('passport');
const {buildProfile}         = require('../user/util');
const passportConfig         = require('./passport.config');
const linkedinStrategy       = require('./strategies/linkedin');

module.exports = function (app) {
  app.get('/oauth/linkedin',
    passport.authenticate('linkedin', {state: 'cyza'})
  );
  
  app.get('/oauth/linkedin/callback',
    passport.authenticate('linkedin', {failureRedirect: '/#/login'}),
    
    function (req, res) {
      const user = req.user;
      
      res.cookie('accountStatus', user.accountStatus);
      
      if (user.accountStatus === 'complete') {
        res.redirect(`/#/user/profile`);
      }
      
      if (user.accountStatus === 'signup') {
        const queryString = qs.stringify(buildProfile(user)) || '';
        res.redirect(`/#/signup/finalize?${queryString}`);
      }
    }
  );
};
