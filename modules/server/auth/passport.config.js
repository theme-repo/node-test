const passport = require('passport');
const User     = require('../user/models/user.model');

passport.serializeUser(function (user, done) {
  if (user) done(null, user._id);
  else {
    console.log('No user');
    done('No User');
  }
});

passport.deserializeUser(function (user, done) {
  User
    .findOne({_id: user})
    
    .exec()
    
    .then(doc => {
      if (doc) done(null, doc);
      else done('No user found');
    });
});